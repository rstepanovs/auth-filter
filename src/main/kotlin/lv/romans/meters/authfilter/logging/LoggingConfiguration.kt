package lv.romans.meters.authfilter.logging

import lv.romans.meters.commons.Constants.LOG_CORRELATION_ID
import lv.romans.meters.commons.Constants.LOG_USER_ID
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import org.springframework.web.filter.CommonsRequestLoggingFilter
import org.springframework.web.servlet.HandlerInterceptor
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter
import org.springframework.web.util.ContentCachingRequestWrapper
import java.util.logging.Level
import java.util.logging.Logger
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.annotation.WebFilter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class CustomHandlerInterceptor(@Value("\${spring.application.name}") val serviceName: String)
    : Filter {

    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        require(request is HttpServletRequest)
        val correlationId = request.getHeader(LOG_CORRELATION_ID)
        val userId = request.getHeader(LOG_USER_ID)
        Logger.getLogger(serviceName).log(Level.INFO,
                "Request with id: ($correlationId) and user id ($userId)")
        try {
            chain.doFilter(request, response)
        } catch (e: Exception) {
            Logger.getLogger(serviceName).log(Level.SEVERE,
                    "Request with id: ($correlationId) and user id ($userId)" +
                            "\n Error: (${e.message})")
            throw Exception(e.message)
        }

    }
}