package lv.romans.meters.authfilter

import lv.romans.meters.commons.Constants
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class AuthorizationFilter(authManager: AuthenticationManager) : BasicAuthenticationFilter(authManager) {

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        val email: String = request.getHeader(Constants.HEADER_USER_ID)
        val role: String = request.getHeader(Constants.HEADER_USER_ROLES)
        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken(email, null, listOf(SimpleGrantedAuthority(role)))
        chain.doFilter(request, response)
    }
}