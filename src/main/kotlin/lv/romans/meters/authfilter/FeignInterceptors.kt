package lv.romans.meters.authfilter

import feign.RequestInterceptor
import feign.RequestTemplate
import lv.romans.meters.commons.Constants
import lv.romans.meters.commons.Constants.LOG_CORRELATION_ID
import lv.romans.meters.commons.Constants.LOG_USER_ID
import org.slf4j.Logger
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import java.util.logging.Level

@Component
class FeignHeaderInterceptor() : RequestInterceptor {

    override fun apply(template: RequestTemplate) {
        val user =
                SecurityContextHolder.getContext().authentication as UsernamePasswordAuthenticationToken?
        val currentRequest = (RequestContextHolder.currentRequestAttributes() as ServletRequestAttributes).request
        if (user != null) {
            template.header(Constants.HEADER_USER_ID, user.name)
                    .header(Constants.HEADER_USER_ROLES, user.authorities.first().authority)
                    .header(LOG_CORRELATION_ID, currentRequest.getHeader(LOG_CORRELATION_ID))
                    .header(LOG_USER_ID, currentRequest.getHeader(LOG_USER_ID))
        }
    }
}